import {arr} from "./Person/Person"
import Person from "./Person/Person";
import './Person/Person.css';
import {Component} from "react";

console.log(arr);


class App extends Component {
    state ={
        numbers: arr,
    }
    num = () =>{
        const newArray = [];

        const max = 36;
        const min = 5;
        while (true){
            const random = Math.floor(Math.random() * (max - min) ) + min;
            newArray.push(random);
            if(newArray.length === 5) {
                break;
            }if(newArray.includes(random)){
                continue;
            }

        }
        newArray.sort((a, b) => a > b ? 1 : -1)

        console.log(this);
        this.setState({
            numbers: newArray,
        });

    }
    render() {
        return (
            <div className="App">
                <div className="nim">{this.state.numbers[0]}</div>
                <div className="nim">{this.state.numbers[1]}</div>
                <div className="nim">{this.state.numbers[2]}</div>
                <div className="nim">{this.state.numbers[3]}</div>
                <div className="nim">{this.state.numbers[4]}</div>
                <div><button className="btn" onClick={this.num}>Click</button></div>

            </div>
        );
    }
}

export default App;
